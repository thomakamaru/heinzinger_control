# %%
"""
    Device class for Heinzinger PNC power supply.

    The Heinzinger Digital Interface I/II is used for many Heinzinger power units.
    Manufacturer homepage:
    https://www.heinzinger.com/products/accessories-and-more/digital-interfaces/

    The Heinzinger PNC series is a series of high voltage direct current power supplies.
    The class HeinzingerPNC is tested with one Heinzinger PNC 60000
    Manufacturer homepage:
    https://www.heinzinger.com/products/high-voltage/universal-high-voltage-power-supplies/

"""

import socket
import logging
from time import sleep
import sys


class HeinzingerDevice():
    """
    Heinzinger Device Class

    Sends basic SCPI commands and reads the answer.
    Only the standard instruction set from the manual is implemented.

    Heinzinger specifies a total reaction time of at least 15ms for communication.
    When a voltage is set, this value is even higher (reaction time of voltage increase etc.)
    Hence, usually 0.1 s = 100 ms of idle time is implemented between sending a message and querying the response.
    """

    def __init__(self, address: str = '192.168.1.1', port: int = 7):
        """
        :param address: string of IP adress xxx.xxx.x.x
        :param port: int of port number e.g. 7
        """
        # IP and Port are standard config. Can be adjusted)
        # Fallback max voltage value if the user does not specify the limit in volts
        self.max_voltage = 60000
        # Fallback max current value if the user does not specify the limit in mA
        self.max_current = 1000
        # standard idle time
        self.wait_time = 0.1

        print("Connecting to: " + address + ":" + str(port))
        try:
            self.device = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.device.connect((address, port))
            msg_to = "IDN?"
            self.device.sendall(msg_to.encode())
            sleep(self.wait_time)
            print("PNC identifies as: " + str(self.read_answer()))
        except ConnectionRefusedError:
            print("PNC was not reached. Choose the correct IP as it set in the Ethernet Options -> TCP/IP v4 of the Private Network between this PC and the Heinzinger Device. Also check the port number.")
            # sys.exit(1)

    def read_answer(self) -> str:
        """
        Reads from the device, until a non-empty line
        is found, or the number of attempts is exceeded.

        :return: String read from the device.
        """
        answer = ""
        attempts = 1
        while len(answer) == 0 and attempts > 0:
            sleep(self.wait_time)
            answer = self.device.recv(1024).decode()
            #print(answer)
            attempts -= 1
        return answer

    def identify(self):
        """
        Queries the IDN? call - identification of PNC source
        """
        msg_to = "IDN?"
        self.device.sendall(msg_to.encode())
        sleep(self.wait_time)
        return self.read_answer()

    def set_voltage(self, value):
        """
        Sets the output voltage VALUE of the Heinzinger PNC to the given value.
        :param value: float of voltage value e.g. 1000 V
        """
        if value <= self.max_voltage:
            msg_to = "VOLT " + str(value)
            self.device.sendall(msg_to.encode())
            sleep(self.wait_time)
            self.read_answer()
            print("SET VOLTAGE\t\t=\t" + self.query_set_voltage() + " \t V")


        else:
            print("VOLTAGE = " + str(value) +
                  " V is too high. Maximum allowed voltage: " + str(self.max_voltage) + " V.")

    def set_current_limit(self, value):
        """
        Sets the output current LIMIT of the Heinzinger PNC to the given value.
        It is necessary to set the current limit in order to generate a voltage > 400 V.

        :param value: float of current limit in mA e.g. 0.1 mA
        """
        if value <= self.max_current:
            msg_to = "CURR " + str(value)
            self.device.sendall(msg_to.encode())
            sleep(self.wait_time)
            print("SET CURRENT = " + str(self.read_answer()) + " V")
        else:
            print("CURRENT = " + str(value) +
                  " mA is too high. Maximum allowed current: " + str(self.max_current) + " mA.")

    def measure_voltage(self):
        """
        Measures the output voltage of the Heinzinger PNC.
        """
        msg_to = "MEAS:VOLT?"
        self.device.sendall(msg_to.encode())
        sleep(self.wait_time)
        print("MEASURED VOLTAGE\t=\t" + self.read_answer() + "\t V")
        return 

    def query_set_voltage(self):
        """
        Queries the PNC for the set voltage. 
        This is not the measurement!
        """
        msg_to = "VOLT?"
        self.device.sendall(msg_to.encode())
        sleep(self.wait_time)
        return self.read_answer()

    def pnc_start(self):
        """
        Starts the voltage of PNC
        """
        msg_to = 'OUTP ON'
        self.device.sendall(msg_to.encode())
        sleep(self.wait_time)
        print(self.read_answer())

    def pnc_stop(self):
        """
        Stops the voltage output of PNC
        """
        self.set_voltage(0)
        sleep(self.wait_time)
        msg_to = 'OUTP OFF'
        self.device.sendall(msg_to.encode())
        print(self.read_answer())

    def reset(self):
        """
        RESETs the the PNC (to local control)
        """
        msg_to = '*RST'
        self.device.sendall(msg_to.encode())
        sleep(self.wait_time)
        self.read_answer()

    def voltage_ramp(self, target_voltage, timestep, number_step: float):
        """
        Builds a symmetric voltage ramp. step_voltage = target_voltage/number_step
        The last voltage value is a hold value. Voltage will NOT be shut off.

        :param target_voltage:  float of highest voltage the PNC should deliver in V e.g. 1000 V
        :param timestep:        float of time one step lasts in s e.g. 1 s
        :param number_step:     int of number of steps e.g. 10 steps
        """
        step = 1
        step_voltage = 0
        self.pnc_start()
        while step <= number_step:
            # uniform split of the total voltage
            step_voltage = int(step * (target_voltage / number_step))
            self.set_voltage(step_voltage)
            sleep(timestep)
            self.measure_voltage()
            step = step + 1

    def set_current_limit_value(self, limit: float):
        """
        Overwrites the current limit value specified in the __init__
        :param limit:  float of highest current the PNC should deliver in mA e.g. 500 mA
        """
        self.max_current = limit

    def set_voltage_limit_value(self, limit: float):
        """
        Overwrites the voltage limit value specified in the __init__
        :param limit:  float of highest voltage the PNC should deliver in V e.g. 30000 V
        """
        self.max_voltage = limit

#s is the class object of the Heinzinger Device; the code prevents double initialization which would lead to an error
try:
    s
except NameError:
    s = HeinzingerDevice('192.168.1.1', 7)

#current and voltage limit has to be set; otherwise standard values are utilized
s.set_current_limit_value(500)
s.set_voltage_limit_value(10000)

#this is a sample code for the voltage ramp: 1000V in 5 steps with 0.1 s between each step
s.voltage_ramp(1000, 0.5, 10)

#this is a sample code for a single voltage value that will be set
s.set_voltage(2000)
s.pnc_start()


sleep(15)
#this is a sample code that prints the currently measured voltage 
s.measure_voltage()

sleep(5)
#this sets the output value to zero and stops PNC output
s.pnc_stop()

